import io
import os
import flask
import requests

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

# explicitly create credentials
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'ServiceAccountKey.json'

# Instantiates a client
client = vision.ImageAnnotatorClient()

# The name of the image file to annotate
file_name = os.path.abspath(r"C:\Users\cps_f\PycharmProjects\Api-Vision\resources\wakeupcat.jpg")

# Loads the image into memory
with io.open(file_name, 'rb') as image_file:
    content = image_file.read()

image = types.Image(content=content)

# Performs label detection on the image file
response = client.label_detection(image=image)
labels = response.label_annotations

print('Labels:')
for label in labels:
    print(label.description)

app = flask.Flask(__name__)


@app.route("/")
def list_labels():
    return label.description


@app.route("/hacker_news_encoding")
def hacker_news_encoding():
    response = requests.get("https://news.ycombinator.com")
    return response.headers["Content-Ecoding"]


if __name__ == "__main__":
    app.run()
