import pytest

from unittest import mock


@pytest.fixture
def default_event_data():
   return {'name': 'Wizz Marathon 2019', 'event-type': 'sport'}


@pytest.mark.django_db
@mock.patch('service.ThirdPartyService.send_new_event')
def test_send_new_event_service_called(
   mock_send_new_event, default_event_data, api_client
):
   response = api_client.post(
       'create-service', data=default_event_data
   )
   assert response.status_code == 201
   assert response.data['id']
   mock_send_new_event.assert_called_with(
       event_id=response.data['id']
   )